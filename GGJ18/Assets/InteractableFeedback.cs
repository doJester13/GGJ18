﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableFeedback : MonoBehaviour {

	public GameObject parent;
	public GameObject feedbackPrefab;

	private GameObject prefabInstance;

	// Use this for initialization
	void Start () {
		LevelManager.Instance.enterInteractionArea += onEnterInteractionArea;
		LevelManager.Instance.exitInteractionArea += onExitInteractionArea;
	}
		
	void onEnterInteractionArea() {
		prefabInstance = Instantiate (feedbackPrefab, parent.transform);
	}

	void onExitInteractionArea() {
		if (prefabInstance != null) {
			Destroy (prefabInstance);
		}
	}
}
