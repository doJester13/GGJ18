﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public AudioSource menuMusic;
	public AudioSource gameMusic;

	private static SoundManager _instance;

	public static SoundManager Instance { get { return _instance; } }

	private void Awake() {
		if (_instance != null) {
			Destroy(this.gameObject);
		} else {
			_instance = this ;
		}
		DontDestroyOnLoad (gameObject);
	}



}
