﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractibleObject : MonoBehaviour {
	
	Collider playerCollider;
	bool isCoroutineExecuting = false;

	void OnTriggerEnter(Collider c) {
		if (c.tag == GlobalConstants.PLAYER && playerCollider == null) {
			LevelManager.Instance.interaction += pushCube;
			LevelManager.Instance.onEnterInteractionArea ();
			playerCollider = c;
		}
	}

	void OnTriggerExit(Collider c) {
		if (c.tag == GlobalConstants.PLAYER && playerCollider != null) {
			LevelManager.Instance.interaction -= pushCube;
			LevelManager.Instance.onExitInteractionArea ();
			playerCollider = null;
		}
	}

	void pushCube() {
		Vector3 raycastInitialPos = transform.position - new Vector3(0, 0.25f, 0);
		Vector3 pushDirection = Vector3.zero;
		RaycastHit hit;
		if (Physics.Raycast (raycastInitialPos, Vector3.forward, out hit, 1)) {
			if (hit.collider.name == "Kiddo") {
				pushDirection = -Vector3.forward;
			}
		}
		if (Physics.Raycast (raycastInitialPos, Vector3.right, out hit, 1)) {
			if (hit.collider.name == "Kiddo") {
				pushDirection = -Vector3.right;
			}
		}
		if (Physics.Raycast (raycastInitialPos, -Vector3.forward, out hit, 1)) {
			if (hit.collider.name == "Kiddo") {
				pushDirection = Vector3.forward;
			}
		}
		if (Physics.Raycast (raycastInitialPos, -Vector3.right, out hit, 1)) {
			if (hit.collider.name == "Kiddo") {
				pushDirection = Vector3.right;
			}
		}

		if (!isCoroutineExecuting) {
			isCoroutineExecuting = true;
			StartCoroutine (moveCube (transform.position + pushDirection));
		}
	}

	IEnumerator moveCube(Vector3 destination) {
		while (Vector3.Distance(transform.position, destination) > 0.1f) {
			transform.position = Vector3.Lerp (
				transform.position,
				destination,
				Time.deltaTime * 10
			);
			yield return null;
		}
		isCoroutineExecuting = false;
	}

}
