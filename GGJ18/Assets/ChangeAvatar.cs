﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeAvatar : MonoBehaviour {

	public List<Sprite> spriteList;

	public Image img;

	private int imgIndex = 0;

	void Start(){
		LevelManager.Instance.powerSwitch += changeAvatar;
		img.sprite = spriteList [0];
	}

	void changeAvatar(){
		imgIndex++;
		if (imgIndex == 3) {
			imgIndex = 0;
		}
		img.sprite = spriteList[imgIndex];
	}

}
