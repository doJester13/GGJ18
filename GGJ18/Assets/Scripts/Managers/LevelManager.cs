﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

	public Transform kiddo;
	public Transform shadow;

	public bool isKiddoActive;

	public StaminaBar stamina;
	public Kiddo playerStatistic;
	
	public delegate void GameEvent();

	public event GameEvent gameOver;
	public event GameEvent playerSwap;
	public event GameEvent playerJump;
	public event GameEvent powerSwitch;
	public event GameEvent staminaOver;
	public event GameEvent interaction;
	public event GameEvent enterInteractionArea;
	public event GameEvent exitInteractionArea;
	
	private static LevelManager _instance;

	public static LevelManager Instance { get { return _instance; } }


	private void Awake()
	{
		if (_instance != null)
		{
			Destroy(this.gameObject);
		} else {
			_instance = this ;
		}
	}


	void Start() {

		InputManager.Instance.yPadEvent += onActiveToggleEvent;
		InputManager.Instance.aPadEvent += sendPlayerJump;
		InputManager.Instance.lbPadEvent += onPowerSwitch;
		InputManager.Instance.xPadEvent += onInteraction;
	}

	void OnDestroy() {
		if (InputManager.Instance != null) {
			InputManager.Instance.ltPadEvent -= onActiveToggleEvent;
			InputManager.Instance.aPadEvent -= sendPlayerJump;
			InputManager.Instance.lbPadEvent -= onPowerSwitch;
			InputManager.Instance.rtPadEvent -= onInteraction;
		}
	}

	public Transform getActivePlayer() {
		return isKiddoActive ? kiddo : shadow;
	}

	public Transform getInactivePlayer() {
		return isKiddoActive ? shadow : kiddo;
	}

	public void toggleActivePlayer() {
		this.isKiddoActive = !this.isKiddoActive;
	}
		
	public void onActiveToggleEvent() {
		playerSwap ();
		toggleActivePlayer ();
		if (!isKiddoActive)
			stamina.consume();
		else{
			StartCoroutine(coolDown());
		}
	}

	public void sendPlayerJump() {
		if (playerJump != null) {
			playerJump ();
		}
	}

	public void notifyGameOver(){
		Time.timeScale = 0.1f;
		if (gameOver != null){
			gameOver();
		}
	}

	public void onPowerSwitch() {
		if (powerSwitch != null) {
			powerSwitch ();
		}
	}

	public void onStaminaOver() {
		if (staminaOver != null) {
			staminaOver ();
		}
	}

	public void onInteraction() {
		if (interaction != null) {
			interaction ();
		}
	}

	public void onEnterInteractionArea() {
		if (enterInteractionArea != null) {
			enterInteractionArea ();
		}
	}

	public void onExitInteractionArea() {
		if (exitInteractionArea != null) {
			exitInteractionArea ();
		}
	}

	IEnumerator coolDown(){
		yield return new WaitForSeconds(1.5f);
		stamina.resane();
	}

}
