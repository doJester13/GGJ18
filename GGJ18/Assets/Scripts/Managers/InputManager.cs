using UnityEngine;

public class InputManager : MonoBehaviour{
	
	private static InputManager _instance;

	public static InputManager Instance { get { return _instance; } }


	private void Awake()
	{
		if (_instance != null)
		{
			Destroy(this.gameObject);
		} else {
			_instance = this ;
		}
	}

    public delegate void buttonClick();
	//keyword event
//    public event buttonClick spaceBarEvent;
//    public event buttonClick wEvent;
//    public event buttonClick sEvent;
//    public event buttonClick aEvent;
//    public event buttonClick dEvent;
//    public event buttonClick qEvent;
//    public event buttonClick eEvent;
//    public event buttonClick shiftEvent;
//    public event buttonClick ctrlEvent;
	//pad event
    public event buttonClick aPadEvent;
	public event buttonClick bPadEvent;
	public event buttonClick xPadEvent;
	public event buttonClick yPadEvent;
//	public event buttonClick jsxPadEvent;
//	public event buttonClick jdxPadEvent;
	public event buttonClick startPadEvent;
	public event buttonClick backPadEvent;
	public event buttonClick lbPadEvent;
	public event buttonClick rbPadEvent;
	public event buttonClick ltPadEvent;
	public event buttonClick rtPadEvent;


    void Update(){
//        if (spaceButton() && spaceBarEvent!= null) spaceBarEvent();
//        if (wButton() && wEvent != null) wEvent();
//        if (sButton() && sEvent != null) sEvent();
//        if (aButton() && aEvent != null) aEvent();
//        if (dButton() && dEvent != null) dEvent();
//        if (qButton() && qEvent != null) qEvent();
//        if (eButton() && eEvent != null) eEvent();
//        if (shiftButton() && shiftEvent != null) shiftEvent();
//        if (ctrlButton() && ctrlEvent != null) ctrlEvent();
        if (aPad() && aPadEvent != null) aPadEvent();
		if (bPad() && bPadEvent != null) bPadEvent();
		if (xPad() && xPadEvent != null) xPadEvent();
		if (yPad() && yPadEvent != null) yPadEvent();
//		if (jsxPad() && jsxPadEvent != null) jsxPadEvent();
//		if (jdxPad() && jdxPadEvent != null) jdxPadEvent();
		if (startPad() && startPadEvent != null) startPadEvent();
		if (backPad() && backPadEvent != null) backPadEvent();
		if (lbPad() && lbPadEvent != null) lbPadEvent();
		if (rbPad() && rbPadEvent != null) rbPadEvent();
		if (ltPad() && ltPadEvent != null) ltPadEvent();
		if (rtPad() && rtPadEvent != null) rtPadEvent();
    }

    // -- Test

    public void test(){
        Debug.Log("test");
    }

    // -- Keyboard

    public bool wButton(){
        return Input.GetKeyDown(KeyCode.W);
    }

    public bool sButton(){
        return Input.GetKeyDown(KeyCode.S);
    }

    public bool aButton(){
        return Input.GetKeyDown(KeyCode.A);
    }

    public bool dButton(){
        return Input.GetKeyDown(KeyCode.D);
    }

    public bool spaceButton() {
        return Input.GetKeyDown(KeyCode.Space);
    }

    public bool qButton(){
        return Input.GetKeyDown(KeyCode.Q);
    }

    public bool eButton(){
        return Input.GetKeyDown(KeyCode.E);
    }

    public bool shiftButton(){
        return Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift);
    }

    public bool ctrlButton(){
        return Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl);
    }

    // -- Pad

    public bool aPad() {
        return Input.GetButtonDown("A_Pad");
    }
	public bool bPad() {
		return Input.GetButtonDown("B_Pad");
	}
	public bool xPad() {
		return Input.GetButtonDown("X_Pad");
	}
	public bool yPad() {
		return Input.GetButtonDown("Y_Pad");
	}
//	public bool jsxPad() {
//		return Input.GetButtonDown("Jsx_Pad");
//	}
//	public bool jdxPad() {
//		return Input.GetButtonDown("Jdx_Pad");
//	}
	public bool startPad() {
		return Input.GetButtonDown("Start_Pad");
	}
	public bool backPad() {
		return Input.GetButtonDown("Back_Pad");
	}
	public bool lbPad() {
		return Input.GetButtonDown("LB_Pad");
	}
	public bool rbPad() {
		return Input.GetButtonDown("RB_Pad");
	}
	public bool ltPad() {
		return Input.GetButtonDown("LT_Pad");
	}
	public bool rtPad() {
		return Input.GetButtonDown("RT_Pad");
	}
		
	// -- Axis

	public float sxHorizontal(){
		float r = 0.0f;
		r += Input.GetAxis ("SX_Horizontal");
		return Mathf.Clamp(r, -1.0f, 1.0f);
	}

	public float sxVertical(){
		float r = 0.0f;
		r += Input.GetAxis ("SX_Vertical");
		return Mathf.Clamp(r, -1.0f, 1.0f);
	}

	public float dxHorizontal(){
		float r = 0.0f;
		r += Input.GetAxis ("DX_Horizontal");
		return Mathf.Clamp(r, -1.0f, 1.0f);
	}

	public float dxVertical(){
		float r = 0.0f;
		r += Input.GetAxis ("DX_Vertical");
		return Mathf.Clamp(r, -1.0f, 1.0f);
	}

//	public float arrowHorizontal(){
//		float r = 0.0f;
//		r += Input.GetAxis ("ArrowHorizontal_Pad");
//		return Mathf.Clamp(r, -1.0f, 1.0f);
//	}
//
//	public float arrowVertical(){
//		float r = 0.0f;
//		r += Input.GetAxis ("ArrowVertical_Pad");
//		return Mathf.Clamp(r, -1.0f, 1.0f);
//	}
}