﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

	public GameObject HUDPanel;
	public GameObject gameOverPanel;

	private CanvasGroup cg;

	void Start(){
		LevelManager.Instance.gameOver += showGameOverPanel;
		cg = gameOverPanel.GetComponent<CanvasGroup> ();
		cg.alpha = 0;
	}

	void showGameOverPanel(){
		StartCoroutine (fadeInOut ());
	}

	IEnumerator fadeInOut(){
		HUDPanel.gameObject.SetActive (false);
		
		while (cg.alpha < 1) {
			cg.alpha += 0.01f;
			yield return new WaitForEndOfFrame ();
		} 
		yield return new WaitForSeconds (.5f);

		HUDPanel.gameObject.SetActive (true);


//		while (cg.alpha > 0) {
//			cg.alpha -= 0.01f;
//			yield return new WaitForEndOfFrame ();
//		} 
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		Time.timeScale = 1.0f;
	}

	void OnDestroy(){
		if (LevelManager.Instance != null) {
			LevelManager.Instance.gameOver -= showGameOverPanel;
		}
	}
}
