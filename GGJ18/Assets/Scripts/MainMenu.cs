﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public void newGame(){
        SceneManager.LoadScene(GlobalConstants.FIRSTLEVEL, LoadSceneMode.Single);
    }

    public void quit(){
        Application.Quit();
    }
}
