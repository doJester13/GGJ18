﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManagerTester : MonoBehaviour {

    private void Start(){
//        InputManager.Instance.spaceBarEvent += logSpace;
//        InputManager.Instance.wEvent += logW;
//        InputManager.Instance.sEvent += logS;
//        InputManager.Instance.aEvent += logA;
//        InputManager.Instance.dEvent += logD;
        InputManager.Instance.aPadEvent += logPad;
		InputManager.Instance.bPadEvent += logPadb;
		InputManager.Instance.lbPadEvent += genericTest;
		InputManager.Instance.rbPadEvent += genericTest;
		InputManager.Instance.startPadEvent += genericTest;
		InputManager.Instance.backPadEvent += genericTest;
    }

    public void logSpace() {
        Debug.Log("spacebar");
    }

    public void logW(){
        Debug.Log("w");
    }

    public void logS(){
        Debug.Log("s");
    }

    public void logA(){
        Debug.Log("a");
    }
    public void logD(){
        Debug.Log("d");
    }

    public void logPad(){
        Debug.Log("a dal pad");
    }

	public void logPadb(){
		Debug.Log("b dal pad");
//		Debug.Log(InputManager.Instance.mainHorizontal() + " , " + InputManager.Instance.mainVertical());
	}

	public void genericTest(){
		Debug.Log("genericTest dal pad");
	}


    public void OnDestroy(){
        if (InputManager.Instance != null) {
//            InputManager.Instance.spaceBarEvent -= logSpace;
//            InputManager.Instance.wEvent -= logW;
//            InputManager.Instance.sEvent -= logS;
//            InputManager.Instance.aEvent -= logA;
//            InputManager.Instance.dEvent -= logD;
            InputManager.Instance.aPadEvent -= logPad;
			InputManager.Instance.bPadEvent -= logPadb;
			InputManager.Instance.lbPadEvent -= genericTest;
			InputManager.Instance.rbPadEvent -= genericTest;
			InputManager.Instance.startPadEvent -= genericTest;
			InputManager.Instance.backPadEvent -= genericTest;
        }
    }

}
