﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPBar : MonoBehaviour {

	RectTransform bar;
	private float totalLife;
	private float percentage;

	void Start(){
		bar = transform.GetChild (0).GetComponent<RectTransform> ();
		totalLife = bar.sizeDelta.x;
		percentage = bar.sizeDelta.x;
		//consumeperc = totalStamina / LevelManager.Instance.playerStatistic.stamina *
		          //    LevelManager.Instance.playerStatistic.staminaForRage;
	}

	public void setPercentage(float p){
		float amount = totalLife / LevelManager.Instance.playerStatistic.hp * p;
		percentage = amount;
		if(bar != null)
			bar.sizeDelta = new Vector2 (percentage, bar.GetComponent<RectTransform> ().sizeDelta.y);
	}

	public void getDamage(float dmg){
		percentage -= dmg;
		if(bar != null)
			bar.sizeDelta = new Vector2 (percentage, bar.GetComponent<RectTransform> ().sizeDelta.y);
	}

	public void addHp(float life){
		percentage += life;
		if(bar != null)
			bar.GetComponent<RectTransform> ().sizeDelta = new Vector2 (percentage, bar.GetComponent<RectTransform> ().sizeDelta.y);
	}

	public float getPercentage(){
		return percentage;
	}

}
