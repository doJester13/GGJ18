﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaminaBar : MonoBehaviour{

	public float necessaryStamForRage;

	RectTransform bar;
	private float totalStamina;
	private float percentage;
	private float consumeperc;
	private float restoreperc;

	private bool decreseStamina = false;

	void Start(){
		bar = transform.GetChild (0).GetComponent<RectTransform> ();
		totalStamina = bar.sizeDelta.x;
		percentage = bar.sizeDelta.x;
		necessaryStamForRage = totalStamina / LevelManager.Instance.playerStatistic.stamina *
		              LevelManager.Instance.playerStatistic.staminaForRage;

		consumeperc = necessaryStamForRage;

		restoreperc = consumeperc;
	}

	public void useStamina(float p){
		percentage -= p;
		bar.sizeDelta = new Vector2 (percentage, bar.GetComponent<RectTransform> ().sizeDelta.y);
	}

	public void addStamina(float p){
		percentage += p;
		bar.GetComponent<RectTransform> ().sizeDelta = new Vector2 (percentage, bar.GetComponent<RectTransform> ().sizeDelta.y);
	}

	public void consume(){
		decreseStamina = true;
	}
	
	public void resane(){
		decreseStamina = false;
	}

	public float getPercentage(){
		return percentage;
	}

	void Update(){
		if (decreseStamina){
			useStamina(consumeperc * Time.deltaTime);

			if (percentage <= 0){
				//decreseStamina = false;
				LevelManager.Instance.onStaminaOver();
				if (!LevelManager.Instance.isKiddoActive){
					LevelManager.Instance.onActiveToggleEvent();
				}
			}
		}
		if (LevelManager.Instance.isKiddoActive && percentage < totalStamina){
			addStamina(restoreperc * Time.deltaTime);
		}
	}
}
