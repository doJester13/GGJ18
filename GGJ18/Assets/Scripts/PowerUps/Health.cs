﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : PowerUps{

    public float ammount = 100;
    public override void effect(KiddoDetectCollision kiddoPlayer){
        base.effect(kiddoPlayer);
        kiddoPlayer.totalHp = ammount;
        
        Destroy(gameObject);
    }
}
