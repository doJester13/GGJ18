﻿using System;

public class GlobalConstants{
	
	//Scene
	public const string FIRSTLEVEL = "TestScene";

	//level consts
	public const string PLAYER = "Player";
	public const string ENEMY = "Enemy";
	
	//powerups
	public const string POWERUPS = "PowerUps";
}