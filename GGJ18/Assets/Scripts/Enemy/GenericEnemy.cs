﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GenericEnemy : MonoBehaviour{
   public List<Transform> positionsTransform;

   public float defaultDamge = 10;
   public float defaultHp = 100;
   public float damageCoolDown = 0.5f;
   public float multiplyBy = 5;
   public float viewDistance = 5;

   public GameObject deathPrefab;
   private NavMeshAgent agent;

   private Transform startTransform;
   private Transform target;
   private Transform shadow;

   private bool isAlert = false;
   private int actualDest = 0;
   private List<Vector3> points = new List<Vector3>();

   private void Start(){
      agent = GetComponent<NavMeshAgent>();
		if (LevelManager.Instance != null) {
			target = LevelManager.Instance.kiddo;
			shadow = LevelManager.Instance.shadow;
		}
      populatePoints();

      if (points.Count>0){
         agent.destination = points[0];
      }
   }


   	private void Update(){
		Emotions selectedEmotion = target.parent.gameObject.GetComponent<MovePlayerTest> ().getSelectedEmotion ();
		bool isEmotionActive = target.parent.gameObject.GetComponent<MovePlayerTest> ().getIsEmotionActive ();
      	//sentinel
      	if (!isAlert){
        	if (Vector3.Distance(transform.position, agent.destination) <= 1){
            	goNextDest();
        	}
	        //alert logic
	        if (Vector3.Distance(transform.position, shadow.position) < viewDistance){
	            RaycastHit hit;
	            if (Physics.Raycast(transform.position, transform.forward, out hit)){
	               	if (hit.collider.CompareTag(GlobalConstants.PLAYER)){
						if (!isEmotionActive || selectedEmotion != Emotions.FEAR) {
							isAlert = true;
						}
	               	}
	            }
	         }
      	}

 
		//follow and run back
      	if (isAlert){
        	if (!LevelManager.Instance.isKiddoActive){
           		if (Vector3.Distance(transform.position, shadow.position) < 2){
              		RunFrom();
              		//try to fight?
               		//strange animation?
            	}
        	} else {
            	agent.destination = target.position;
        	}
      	}

   }

   public void getDamage(float dmg){
       defaultHp -= dmg;
       checkForDeath();
   }

   private void checkForDeath(){
       if (defaultHp <= 0){
			die();
		}
   }

   private void die(){
		Instantiate(deathPrefab, transform.position, Quaternion.identity);
   		Destroy(gameObject);
   }

   private void populatePoints(){
   		foreach (Transform t in positionsTransform){
			points.Add(t.position);
         }
   }

   private void goNextDest(){
      if (actualDest == 0){
         if (points.Count > 1){
            agent.destination = points[1];
            actualDest = 1;
         }    
      }else if (actualDest == 1){
         if (points.Count>0){
            agent.destination = points[0];
            actualDest = 0;
         }
      }
   }

   private void RunFrom(){
 
 
      startTransform = transform;
         
      transform.rotation = Quaternion.LookRotation(transform.position - shadow.position);
 
      Vector3 runTo = transform.position + transform.forward * Random.Range(1, multiplyBy);
 
      NavMeshHit hit;    

      NavMesh.SamplePosition(runTo, out hit, 5, 1 << NavMesh.GetAreaFromName("Walkable")); 
      
      transform.position = startTransform.position;
      transform.rotation = startTransform.rotation;
 
      agent.destination = hit.position;
   }

}
