﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayOnPlayer : MonoBehaviour {

	public Vector3 offset;

	void Update() {
		if (LevelManager.Instance != null) {
			transform.position = Vector3.Lerp (
				transform.position,
				LevelManager.Instance.getActivePlayer ().position + offset,
				Time.deltaTime
			);
		}
	}

}
