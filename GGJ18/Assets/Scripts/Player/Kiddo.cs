﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "KiddoData", menuName = "GGJ18/CreateKiddo", order = 1)]
public class Kiddo : ScriptableObject{
    public float hp = 100;
    public float stamina = 70;

    public float staminaForRage = 3;
}
