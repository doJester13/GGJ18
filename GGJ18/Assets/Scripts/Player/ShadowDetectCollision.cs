﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowDetectCollision : MonoBehaviour {

    private void OnTriggerEnter(Collider other){
		if (!LevelManager.Instance.isKiddoActive
			&& LevelManager.Instance.kiddo.parent.GetComponent<MovePlayerTest>().getSelectedEmotion() == Emotions.ANGER
		){
            if (other.CompareTag(GlobalConstants.ENEMY)){
                GenericEnemy enemy = other.gameObject.GetComponent<GenericEnemy>();
                enemy.getDamage(enemy.defaultHp);
            }
        }
    }
}
