﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KiddoDetectCollision : MonoBehaviour{

    public HPBar hpBar;
    
    private float _totalHP = 100;
    public float totalHp{
        get{ return _totalHP; }
        set{
            _totalHP = value; 
            hpBar.setPercentage(totalHp);
        }
    }

    private float SavedTime = 0;
    
    void Start(){
        totalHp = LevelManager.Instance.playerStatistic.hp;
    }
     
    private void OnTriggerStay(Collider other){

        if (other.CompareTag(GlobalConstants.ENEMY)){
            GenericEnemy enemy = other.gameObject.GetComponent<GenericEnemy>();
            if ((Time.time - SavedTime) > enemy.damageCoolDown){
                SavedTime = Time.time;
                totalHp -= enemy.defaultDamge;
                checkForGameOver();
            }
        }
    }
    
    private void OnTriggerEnter(Collider other){
        if (other.CompareTag(GlobalConstants.ENEMY)){
            GenericEnemy enemy = other.gameObject.GetComponent<GenericEnemy>();   
            totalHp -= enemy.defaultDamge;
//            checkForGameOver();
			LevelManager.Instance.notifyGameOver();
        }
        
        if (other.CompareTag(GlobalConstants.POWERUPS)){
            other.GetComponent<PowerUps>().effect(this);
        } 
    }

    private void checkForGameOver(){
        if (totalHp <= 0){
            LevelManager.Instance.notifyGameOver();
        }
    }
}
