﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Emotions {
	ANGER,
	FEAR,
	JOY
};

public class MovePlayerTest : MonoBehaviour {

	public float speed;
	public float tiltAngle = 15.0f;
	public float rotationTime = 2.0f;
	public float jumpHeight;
	public float jumpSpeed;
	public Animator kiddoAnimator;
	public Animator shadowAnimator;

	private bool isJumping;
	private float jumpDestHeight;

	private Emotions selectedEmotion = Emotions.ANGER;
	private bool isEmotionActive = false;

	public Emotions getSelectedEmotion() {
		return selectedEmotion;
	}

	public bool getIsEmotionActive() {
		return isEmotionActive;
	}

	private Transform getActivePlayer() {
		if (isEmotionActive && selectedEmotion != Emotions.FEAR) {
			return LevelManager.Instance.shadow;
		} else {
			return LevelManager.Instance.kiddo;
		}
	}

	private Transform getInactivePlayer() {
		return getActivePlayer ().gameObject.name == "Kiddo" ? LevelManager.Instance.shadow : LevelManager.Instance.kiddo;
	}

	void Start() {
		if (LevelManager.Instance != null) {
			LevelManager.Instance.playerSwap += swapPlayer;
			LevelManager.Instance.playerJump += kiddoJump;
			LevelManager.Instance.powerSwitch += powerSwitch;
			LevelManager.Instance.staminaOver += onStaminaOver;
		}
	}

	void OnDestroy(){
		if (LevelManager.Instance != null) {
			LevelManager.Instance.playerSwap -= swapPlayer;
			LevelManager.Instance.playerJump -= kiddoJump;
			LevelManager.Instance.powerSwitch -= powerSwitch;
			LevelManager.Instance.staminaOver -= onStaminaOver;
		}
	}

	void Update () {
		//active player
		Transform activePlayer = getActivePlayer();
		Transform inactivePlayer = getInactivePlayer();
		float mvAmountX = InputManager.Instance.sxHorizontal();
		float mvAmountY = InputManager.Instance.sxVertical();

		Vector3 nextDir = new Vector3 (mvAmountX, 0, mvAmountY);

		if (nextDir != Vector3.zero) {
			activePlayer.transform.rotation = Quaternion.LookRotation (nextDir);
			inactivePlayer.transform.LookAt (new Vector3(activePlayer.position.x, inactivePlayer.position.y, activePlayer.position.z));
		}

		if (!Mathf.Approximately (mvAmountX, 0.0f) || !Mathf.Approximately (mvAmountY, 0.0f)) {
			activePlayer.transform.position = Vector3.Lerp (
				activePlayer.transform.position,
				activePlayer.transform.position + activePlayer.transform.forward * speed,
				Time.deltaTime * speed
			);
			if (activePlayer.name == "Kiddo") {
				kiddoAnimator.SetBool ("isRunning", true);
			} else {
				shadowAnimator.SetBool ("isRunning", true);
			}
		} else {
			if (activePlayer.name == "Kiddo") {
				kiddoAnimator.SetBool ("isRunning", false);
			} else {
				shadowAnimator.SetBool ("isRunning", false);
			}
		}

		if (!isEmotionActive || selectedEmotion != Emotions.JOY) {
			setKiddoGravity (true);
		}

		//inactive player
		if (isEmotionActive) {
			if (selectedEmotion == Emotions.ANGER) {
				Vector3 directionToLook = new Vector3 (activePlayer.transform.position.x, 0, activePlayer.transform.position.z);

				inactivePlayer.transform.rotation = Quaternion.LookRotation (directionToLook);
				kiddoAnimator.SetBool ("isAgony", true);
				shadowAnimator.SetBool ("isAngry", true);
			} else {
				kiddoAnimator.SetBool ("isAgony", false);
				shadowAnimator.SetBool ("isAngry", false);
			}
			if (selectedEmotion == Emotions.FEAR) {
				// Kiddo diventa invisibile
				// L'ombra copre kiddo
				inactivePlayer.transform.position = Vector3.Lerp(
					inactivePlayer.transform.position,
					activePlayer.transform.position,
					Time.deltaTime * speed * 5
				);
				shadowAnimator.SetBool ("isRunning", true);
			}
			if (selectedEmotion == Emotions.JOY) {
				// Kiddo segue l'ombra
				if (mvAmountX != 0.0f || mvAmountY != 0.0f){
					Vector3 kiddoPos = activePlayer.transform.position;
					Vector3 jumpDest = new Vector3 (kiddoPos.x, jumpHeight, kiddoPos.z);
					activePlayer.transform.position = Vector3.Lerp (
						kiddoPos,
						jumpDest,
						Time.deltaTime * jumpSpeed
					);
					inactivePlayer.transform.position = Vector3.Lerp(
						inactivePlayer.transform.position,
						activePlayer.transform.position,
						Time.deltaTime * speed * 5
					);
					// TODO: Flying animation
					shadowAnimator.SetBool ("isRunning", true);
					shadowAnimator.SetBool ("isRunning", true);
				}
			}
		} else {
			kiddoAnimator.SetBool ("isAgony", false);
			shadowAnimator.SetBool ("isAngry", false);
			if (!Mathf.Approximately (mvAmountX, 0.0f) || !Mathf.Approximately (mvAmountY, 0.0f)) {
				inactivePlayer.transform.position = Vector3.Lerp (
					inactivePlayer.transform.position,
					activePlayer.transform.position,
					Time.deltaTime * speed
				);
				shadowAnimator.SetBool ("isRunning", true);
			} else {
				shadowAnimator.SetBool ("isRunning", false);
			}
			if (isJumping) {
				Vector3 kiddoPos = activePlayer.transform.position;
				Vector3 jumpDest = new Vector3 (kiddoPos.x, jumpDestHeight, kiddoPos.z);
				activePlayer.transform.position = Vector3.Lerp (
					kiddoPos,
					jumpDest,
					Time.deltaTime * jumpSpeed
				);
				//TODO: Jumping animation
			}
		}
	}

	void kiddoJump() {
		Transform kiddo = LevelManager.Instance.getActivePlayer ();
		if (
			LevelManager.Instance.isKiddoActive
			&& !isJumping
			&& Physics.Raycast(kiddo.transform.position, Vector3.down, 1)
		) {
			isJumping = true;
			StartCoroutine (stopJumpAfter(0.3f));
			jumpDestHeight = kiddo.transform.position.y + jumpHeight;
		}
	}

	void swapPlayer() {
		isEmotionActive = !isEmotionActive;
		if (selectedEmotion == Emotions.JOY) {
			setKiddoGravity (false);
		}
	}

	void powerSwitch() {
		selectedEmotion = selectedEmotion == Emotions.JOY ? 
			Emotions.ANGER : (selectedEmotion + 1);
		if (isEmotionActive && selectedEmotion == Emotions.JOY) {
			setKiddoGravity (false);
		}
		print (selectedEmotion);
	}

	void onStaminaOver() {
		setKiddoGravity (true);
	}

	void setKiddoGravity(bool active) {
		LevelManager.Instance.kiddo.gameObject.GetComponent<CustomGravity>().enabled = active;
	}

	IEnumerator stopJumpAfter(float seconds) {
		yield return new WaitForSeconds(seconds);
		isJumping = false;
	}

	void setActiveEmotion(Emotions e) {
		selectedEmotion = e;
	}

}
