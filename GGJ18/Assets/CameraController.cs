﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public Vector3 initialRotation;


	void Update () {
		float hMove = Input.GetAxis ("DX_Horizontal");
		transform.rotation = Quaternion.Slerp(
			transform.rotation,
			Quaternion.Euler (new Vector3(
				initialRotation.x,
				initialRotation.y + hMove * 20,
				initialRotation.z
			)),
			Time.deltaTime
		);
	}
}
